<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

// get generic woocommerce email header
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<p><?php esc_html_e( 'Deine Bestellung ist endlich auf dem Weg!', 'woocommerce' ); //TODO - i18n ?></p>  
<blockquote><?php echo wpautop( wptexturize( make_clickable( $track_trace ) ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></blockquote>
<p><?php esc_html_e( 'Als kleine Erinnerung an deine Bestellung:', 'woocommerce' );//TODO - i18n ?></p>

<?php
// get user specific order data
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );


// get generic woocommerce email footer
do_action( 'woocommerce_email_footer', $email );





