<?php 

namespace CSC\Admin;

if( !defined( "ABSPATH") ) {
    exit;
}

class CSC_Settings_Tab {

    //initialize all defined options for the settings tab
    public static function init() {
        add_filter( 'woocommerce_settings_tab_array', __CLASS__. '::add_settings_tab', 50);
        add_action( 'woocommerce_settings_tab_settings_tab_csc', __CLASS__. '::settings_tab' );
        add_action( 'woocommerce_update_options_settings_tab_csc', __CLASS__. '::update_settings' );
    }

    // add settings tab
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_csc'] = __('Custom Status Codes', 'woocommerce-settings-tab-csc');
        return $settings_tabs;
    }

    // add content within the settings tab
    public static function settings_tab() {
        woocommerce_admin_fields( self::get_settings() );
    }

    public static function update_settings() {
        woocommerce_update_options( self::get_settings() );
    }

    public static function get_settings() {
        $settings = array(
            'section_title' => array(
                'name'  => _('Custom Status Code Einstellungen', 'woocommerce-settings-tab-csc'),
                'type'  => 'title',
                'desc'  => '',
                'id'    => 'wc_settings_tab_csc_section_title'
            ),
            'title' => array(
                'name' => _('Titel', 'woocommerce-settings-tab-csc'),
                'type' => 'text',
                'desc' => _('This is some helper text', 'woocommerce-settings-tab-csc'),
                'id'   => 'wc_settings_tab_csc_title'
            ),
            'description' => array(
                'name'  => _('Beschreibung', 'woocommerce-settings-tab-csc'),
                'type'  => 'textarea',
                'desc'  => _('This is a paragraph describing the setting. Lorem ipsum dolor sit amet. Ipsum sit amet lorem dolor.'),
                'id'    => 'wc_settings_tab_csc_description'
            ),
            'section_end' => array(
                'type'  => 'sectionend',
                'id'    => 'wc_settings_tab_csc_section_end'
            )
        );
        
        return apply_filters( 'wc_settings_tab_csc_settings', $settings);
    }

}

//call static initialization
CSC_Settings_Tab::init();

?>