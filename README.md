# WooCommerce Custom Status Codes
This plugin will help you adding custom status codes to WooCommerce for specific requirements. 
Below you will find all relevant information about how to setup and use the plugin. 

### Requirements
- A working WordPress installation
- WooCommerce plugin for WordPress

### Installation
- Go to your "Plugin" section in WordPress and search for new plugins
- Search for "WooCommerce Custom Status Codes" and press enter
- Install the plugin "WooCommerce Custom Status Codes" and activate it

### Usage of this plugin
This section is currently under development 