<?php
/*
Plugin Name: WooCommerce Custom Status Codes
Plugin URI: https://joyments.de
Description: Joyments-spezifische Erweiterungen für WooCommerce. Vordefinierte Statuscodes für verbesserte Kundenkommunikation. Erweitert Wordpress um SMTP-Funktionalität.
Version: 1.0
Author: Dominic Ronke
Author URI: https://de.linkedin.com/in/dominic-ronke-b258b6121
License: GPLv2
*/

if (! defined( "ABSPATH") ) {
	exit;
}

// Load admin
function csc_load() {
	include_once 'admin/csc_settings_tab.php';
}

// Making the new statuses available for WooCommerce via hook
function csc_register_order_post_status( $statuses ) {
	register_post_status( 'wc-produced', array(
		'label'						=> _x('Produziert', 'WooCommerce Order status' ), // TODO - i18n
		'public' 					=> true,
		'exclude_from_search'		=> false,
		'show_in_admin_all_list'	=> true,
		'show_in_admin_status_list'	=> true,
		'label_count'				=> _n_noop( 'Produziert <span class="count">(%s)</span>', 'Produziert <span class="count">(%s)</span>' ), // TODO - i18n
	));
	
	register_post_status( 'wc-in-production', array(
		'label'						=> _x('In Produktion', 'WooCommerce Order status'), // TODO - i18n
		'public' 					=> true,
		'exclude_from_search'		=> false,
		'show_in_admin_all_list'	=> true,
		'show_in_admin_status_list'	=> true,
		'label_count'				=> _n_noop( 'In Produktion <span class="count">(%s)</span>', 'In Produktion <span class="count">(%s)</span>' ), // TODO - i18n
	));
	
	register_post_status( 'wc-shipped', array( 
		'label'						=> _x('Versendet', 'WooCommerce Order status' ), // TODO - i18n
		'public' 					=> true,
		'exclude_from_search'		=> false,
		'show_in_admin_all_list'	=> true,
		'show_in_admin_status_list'	=> true,
		'label_count'				=> _n_noop( 'Versendet <span class="count">(%s)</span>', 'Versendet <span class="count">(%s)</span>' ), 		// TODO - i18n
	));
	
	return $statuses;
}

add_filter( 'init', 'csc_register_order_post_status' );
add_filter( 'woocommerce_register_shop_order_post_statuses', 'csc_register_order_post_status', 10, 1);

// Generate new statuses
function csc_add_order_status( $statuses ) {
	$statuses['wc-in-production'] 	= 'In Produktion'; 	// TODO - i18n
	$statuses['wc-produced']	 	= 'Produziert'; 	// TODO - i18n
	$statuses['wc-shipped'] 		= 'Versendet'; 		// TODO - i18n
	return $statuses;
}
add_filter( 'wc_order_statuses', 'csc_add_order_status', 10, 1);

// Enable wordpress to send mails via own SMTP
add_action('phpmailer_init', 'csc_init_smtp');
function csc_init_smtp( $phpmailer ) {
	$phpmailer->isSMTP();
    $phpmailer->Host       = SMTP_HOST;
    $phpmailer->SMTPAuth   = SMTP_AUTH;
    $phpmailer->Port       = SMTP_PORT;
    $phpmailer->SMTPSecure = SMTP_SECURE;
    $phpmailer->Username   = SMTP_USERNAME;
    $phpmailer->Password   = SMTP_PASSWORD;
    $phpmailer->From       = SMTP_FROM;
    $phpmailer->FromName   = SMTP_FROMNAME;
}

// Send email when order is shipped
function csc_send_tracking_code( $order_id, $order ) {
	// retreive all order notes
	$notes = wc_get_order_notes([
		'order_id' 	=> $order->get_id(),
		'orderby'	=> 'date_created',
		'order'		=> 'DESC'
	]);
	
	$arr_len = count($notes);
	$text = "Hallo " . $order->get_billing_first_name() . ", deine Bestellung ist unterwegs! Verfolge sie hier: ";
	for($i = 0; $i < $arr_len; $i++) {
		// check if valid URL
		if( filter_val( $notes[i]->content , FILTER_VALIDATE_URL ) ) {
			$text = $text . $notes[i]->content;
		}
	}


	wp_mail($order->get_billing_email() ,'Deine Bestellung ist unterwegs!', $text);	// TODO - i18n
}

add_action( 'woocommerce_order_status_shipped', 'csc_send_tracking_code', 10, 2);
?>
